package main

const Start = `Hello!
Choose the country whose holiday you want to receive.`
const ErrHoliday = `We could not find a holiday.
Try again later.`
const ErrCountryCode = `Unexpected country.
Please choose available country.`

func NoHoliday(country string) string {
	return "In " + country + " no holiday today."
}
