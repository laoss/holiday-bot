package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func getHoliday(countryCode string) ([]map[string]string, error) {
	year, month, day := time.Now().Date()
	url := fmt.Sprintf(
		"https://holidays.abstractapi.com/v1/?api_key=%s&country=%s&year=%d&month=%d&day=%d",
		os.Getenv("HOLIDAY_API_PRIVATE_KEY"),
		countryCode,
		year,
		int(month),
		day,
	)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var holidays []map[string]string
	err = json.Unmarshal(body, &holidays)
	if err != nil {
		return nil, err
	}

	length := len(holidays)
	if length == 0 {
		return nil, nil
	}
	return holidays, nil
}
