package main

import (
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tele "gopkg.in/telebot.v3"
	"os"
	"strconv"
	"time"
)

var flags = map[string]string{
	"🇺🇦": "UA",
	"🇺🇸": "US",
	"🇬🇧": "GB",
	"🇲🇨": "PL",
	"🇨🇿": "CZ",
	"🇱🇻": "LV",
	"🇱🇹": "LT",
	"🇪🇪": "EE",
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot load env variables.")
	}

	setupLogger()

	bot, err := setupNewBot()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot setup new bot.")
	}

	commandHandler(bot)

	bot.Start()
}

func setupNewBot() (*tele.Bot, error) {
	pref := tele.Settings{
		Token:  os.Getenv("BOT_TOKEN"),
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}

	b, err := tele.NewBot(pref)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func commandHandler(b *tele.Bot) {
	b.Handle(tele.OnText, func(c tele.Context) error {
		logSender(c)

		switch c.Text() {
		case "/start":
			return startHandler(c)
		default:
			return flagHandler(c)
		}
	})
}

func startHandler(c tele.Context) error {
	var (
		keyboard = &tele.ReplyMarkup{ResizeKeyboard: true}
		btns     []tele.ReplyButton
	)

	for _, i := range flags {
		btn := tele.ReplyButton{Text: i}
		btns = append(btns, btn)
	}

	length := len(btns)
	index := length / 2
	keyboard.ReplyKeyboard = append(keyboard.ReplyKeyboard, btns[:index])
	keyboard.ReplyKeyboard = append(keyboard.ReplyKeyboard, btns[index-1:])

	return c.Send(Start, keyboard)
}

func flagHandler(c tele.Context) error {
	countryCode, ok := flags[c.Text()]
	if !ok {
		return c.Send(ErrCountryCode, tele.ModeHTML)
	}

	holidays, err := getHoliday(countryCode)

	if err != nil {
		log.Error().Msg(err.Error())
		return c.Send(ErrHoliday, tele.ModeHTML)
	} else if holidays == nil {
		return c.Send(NoHoliday(c.Text()), tele.ModeHTML)
	}

	var resp string
	if len(holidays) > 1 {
		for i, v := range holidays {
			resp += v["name"]
			if i < len(holidays)-1 {
				resp += ",\n"
			} else {
				resp += "."
			}
		}
	} else {
		resp = holidays[0]["name"]
	}
	return c.Send(resp, tele.ModeHTML)
}

func setupLogger() {
	if os.Getenv("LOG_PRETTY_ENABLE") == "1" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}
	if os.Getenv("LOG_TIME_FORMAT_UNIX") == "1" {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}
}

func logSender(c tele.Context) {
	var (
		text = c.Text()
		user string
	)
	if c.Sender().Username != "" {
		user = c.Sender().Username
	} else {
		user = strconv.FormatInt(c.Sender().ID, 10) + "  " + c.Sender().FirstName
		if c.Sender().LastName != "" {
			user += " " + c.Sender().LastName
		}
	}

	log.Info().Msgf("user: %s, text: %s", user, text)
}
